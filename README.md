# site
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/e008974b46ca423f8edaf70eaba7768f)](https://app.codacy.com/manual/fgsoftware1/site?utm_source=github.com&utm_medium=referral&utm_content=fgsoftware1/site&utm_campaign=Badge_Grade_Dashboard)

[![Build status](https://ci.appveyor.com/api/projects/status/12dbrbp7qe3mjuqj?svg=true)](https://ci.appveyor.com/project/fgsoftware1/site)

[![Build status](https://ci.appveyor.com/api/projects/status/12dbrbp7qe3mjuqj/branch/master?svg=true)](https://ci.appveyor.com/project/fgsoftware1/site/branch/master)

[![Build Status](https://travis-ci.com/fgsoftware1/site.svg?branch=master)](https://travis-ci.com/fgsoftware1/site)

[![CircleCI](https://circleci.com/gh/fgsoftware1/fgOS/tree/master.svg?style=svg)](https://circleci.com/gh/fgsoftware1/fgOS/tree/master)

[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgithub.com%2Ffgsoftware1%2Fsite.svg?type=shield)](https://app.fossa.io/projects/git%2Bgithub.com%2Ffgsoftware1%2Fsite?ref=badge_shield)

## License
[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgithub.com%2Ffgsoftware1%2Fsite.svg?type=large)](https://app.fossa.io/projects/git%2Bgithub.com%2Ffgsoftware1%2Fsite?ref=badge_large)